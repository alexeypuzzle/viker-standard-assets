﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viker_mainMenu : MonoBehaviour {

    public void onPlayPressed()
    {
        Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);
    }

	public void onSettingsPressed()
	{
        Viker_UIController.instance.LoadUI(Viker_UI.Settings);
	}
}
