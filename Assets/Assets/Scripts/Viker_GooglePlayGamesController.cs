﻿#if UNITY_ANDROID
using UnityEngine;
using GooglePlayGames;

public class Viker_GooglePlayGamesController : MonoBehaviour {

	public static Viker_GooglePlayGamesController instance;
	bool isConnectedToGooglePlayServices;
	bool isConnectedToGameCenter;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{

        PlayGamesPlatform.Activate ();
        PlayGamesPlatform.DebugLogEnabled = true;
        ConnectToGooglePlayServices ();
	}

	public void ConnectToGooglePlayServices()
	{
		if (!isConnectedToGooglePlayServices)
		{
			Social.localUser.Authenticate(success =>
			{
				isConnectedToGooglePlayServices = success;
			});
		}
	}

}
#endif