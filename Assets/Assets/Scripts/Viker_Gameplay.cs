﻿using UnityEngine;

public class Viker_Gameplay : MonoBehaviour {

    public static Viker_Gameplay instance;
    public GameObject GameOverScreen;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ResetGame();
    }

    public void Die(){
        GameOverScreen.SetActive(true);
    }

    public void PlayAgain(){
        ResetGame();
        GameOverScreen.SetActive(false);
    }

    public void ResetGame(){ 
        
    }

    public void onBackButtonPressed(){
        Viker_UIController.instance.GoBack();
    }

	public void onSettingsButtonPressed()
	{
        Viker_UIController.instance.LoadUI(Viker_UI.Settings);
	}
        
}
