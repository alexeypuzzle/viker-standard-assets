﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class Viker_FacebookController : MonoBehaviour {

	public static Viker_FacebookController instance;
	public long facebookAppId = 753352991490707;
	List<string> readPermissions;
	List<string> writePermissions;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
			//Handle FB.Init
			FB.Init(() =>
			{
				FB.ActivateApp();
			});
		}
	}

    public void LoginWithBasicPermissions(){
		if (!FB.IsInitialized)
		{
			FB.Init(delegate ()
				{
					FB.LogInWithReadPermissions(readPermissions, loginCallback);
				});
		}
		else
		{
			FB.LogInWithReadPermissions(readPermissions, loginCallback);
		}
    }

	void loginCallback(ILoginResult result)
	{
		if (result.Error != null || !FB.IsLoggedIn)
		{
			Debug.LogError(result.Error);
        } else{
            Debug.Log("Logged into facebook successfully");
        }
	}

}
