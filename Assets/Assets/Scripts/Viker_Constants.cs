﻿public class Viker_UI
{
    public static readonly string MainMenu = "MainMenu";
    public static readonly string Gameplay = "Gameplay";
    public static readonly string Settings = "Settings";
    public static readonly string DebugMenu = "DebugMenu";
}

public class Viker_URLS
{
    public static readonly string VikerWebsite = "http://viker.co.uk";
}

public class Viker_prefsKeys
{
    public static readonly string DebugMode = "d";
    public static readonly string Music = "m";
    public static readonly string Sound = "s";
}