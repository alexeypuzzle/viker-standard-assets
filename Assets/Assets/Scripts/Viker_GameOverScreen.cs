﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viker_GameOverScreen : MonoBehaviour {

    public void onPlayAgainPressed(){
        Viker_Gameplay.instance.PlayAgain();
    }

	public void onHomePressed()
	{
        Viker_UIController.instance.LoadUI(Viker_UI.MainMenu);
	}

	public void onSharePressed()
	{

	}

}
