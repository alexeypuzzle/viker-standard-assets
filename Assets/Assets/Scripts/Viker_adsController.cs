﻿using UnityEngine;
using admob;

public class Viker_adsController : MonoBehaviour
{
    // add any UNITY ads implementation to this class.

    public string iOSBannerId;
    public string iOSStaticInterstitialId;

    public string AndroidBannerId;
    public string AndroidStaticInterstitialId;

    string _bannerID;
    string _interstitialID;

    private void Awake()
    {
#if UNITY_IOS
        _bannerID = iOSBannerId;
        _interstitialID = iOSStaticInterstitialId;

#elif UNITY_ANDROID
        _bannerID = AndroidBannerId;
        _interstitialID = AndroidStaticInterstitialId;
#endif
	}

    void Start(){
        Admob ad = Admob.Instance();
        ad.initAdmob(_bannerID, _interstitialID);
		ad.bannerEventHandler += onBannerEvent;
		ad.interstitialEventHandler += onInterstitialEvent;
		//ad.rewardedVideoEventHandler += onRewardedVideoEvent;
		//ad.nativeBannerEventHandler += onNativeBannerEvent;
    }

	void onBannerEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
	}

	void onInterstitialEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
		if (eventName == AdmobEvent.onAdLoaded)
		{
			Admob.Instance().showInterstitial();
		}
	}


}
