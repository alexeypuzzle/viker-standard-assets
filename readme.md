# Viker Standard Asset pack

## Known issues (As of Unity 5.6.1f1)
1. Duplicate Facebook.settings.dll files (default unity one vs. Facebook SDK one). Solution: remove Unity's dll (ProjectName/Library/FacebookSDK/Facebook.Unity.Settings.dll) and restart Unity.

2. Android only - minimum SDK version.
If you let Unity generate the main AndroidManifest.xml automatically, it can complain about different minimum SDK numbers. To resolve, open ProjectName/Assets/Plugins/AndroidManifest.xml and change "minSdkVersion" to match the one in Player Settings

3. Android only - "unable to convert classes into dex format"
A result of Android SDKs using different versions of the same classes. To resolve, find duplicates in your project and remove the oldest version.

4. "Google Play Games Services not configured"
To resolve, follow the setup guide: https://github.com/playgameservices/play-games-plugin-for-unity

## Install instructions
1. Download repo
2. Add unity package to existing project
3. Use existing skeleton UI and classes as boilerplate
4. Make awesome things

## Docs

### Google Play Services
https://github.com/playgameservices/play-games-plugin-for-unity

### Admob
https://github.com/unity-plugins/Unity-Admob

### Facebook
The function LoginWithBasicPermissions is exposed on the Facebook manager singleton to get basic user info from Facebook. Add any additional implementations to this class.